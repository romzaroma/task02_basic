package com.epam;
import java.util.Scanner;
/**
 * @version 1.6 13 Nov 2019
 * @autor Roman Zaivyi
 */
 class Application_2 {
    public static void printFibonacciNumbers(int n) {
        int f1 = 0, f2 = 1, i ,dv,ev, d =1 , e =1 , dn=0, en=0, v=100;
        if (n < 1)
            return;
        for (i = 1; i <= n; i++) {
            if (f2 % 2 != 0) {
                d = f2;
                /** maxOdd is the biggest odd
                 *  number of Fibonacci numbers(d).
                 *  Sum of odd numbers(dn)*/
                dn=dn+1;
            }
            else {e = f2;
                /** maxEven is the biggest even
                 * number of Fibonacci numbers(e).
                 * Sum of even numbers(en)*/
                en=en+1; }
            int next = f1 + f2;
            f1 = f2;
            f2 = next;
        }
        System.out.println();
        System.out.println("Max odd :"+ d);
        System.out.println("Max even :"+ e);
        /**
         * Percentage method prints percentage of
         * odd and even Fibonacci numbers.
         */
        dv=(dn*v)/n;
        ev=(en*v)/n;
        System.out.println("Number of odd in percent ="+dv+"%");
        System.out.println("Number of even in percent ="+ev+"%");
    }
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of elements:");
        int z = in.nextInt();
        in.close();
        printFibonacciNumbers(z);
    }
}
