package com.epam;
import java.util.Scanner;
/**
 * @version 1.2 13 Nov 2019
 * @autor Roman Zaivyi
 */
class Application_1 {
    public static void main(String[] args){
        int c , sumOdd = 0, sumEven = 0;
        /**
         * the interval is set via the scanner
         */
        Scanner in = new Scanner(System.in);
        System.out.println("Write the begin of interval:");
        int beginInterval = in.nextInt();
        System.out.println("Write the end of interval:");
        int endInterval = in.nextInt();
        in.close();
        System.out.print("Odd number is:");
        for(c=beginInterval; c<=endInterval; c++){
            if (c%2 != 0){
                sumOdd=sumOdd+c;
                System.out.print(c+" ");
                /**
                 * PrintOddNumber method prints odd numbers from start
                 * the end of interval
                 */
            }
        }
        System.out.println();
        System.out.println("Sum of odd number="+ sumOdd);
        /**
         * PrintSumNumber method prints the sum of odd numbers.
         * PrintEvenNumber method prints even numbers from end
         *  to start of interval
         */
        System.out.print("Even number is:");
        for (c=endInterval; c>=beginInterval ; c--){
            if (c % 2==0){
                sumEven=sumEven+c;
                System.out.print(c+" ");
            }
        }
        System.out.println();
        System.out.println("Sum of even numbers= "+ sumEven);
        /**PrintSumNumber method prints the sum of even numbers.*/
    }
}
